# Latex thesis example
Basic example of Latex document prepared for my collegues - students of University of Economics in Cracow.

## Features

  - Full support of `biblatex` + `biber`
  - Polish fonts support (also for listings package);
  - UTF-8 encoding by default;
  - Implemented [Generall Editor's Rules](#generall-editors-rules);
  - Many class options for easy personal confuguation;
  - Autoconfiguration for male or female formating of thesis;
  - Easy to set global variables;
  - Download repository, compile and have great fun;
  - Ready for Windows and Linux OSs;


## Basic setup

1. [Install neccesary bibraries like: TeXLive or MikTeX](#installation-of-additional-libs)
2. [Copy repository to your hard drive](#copy-repo)
3. [Compile document](#compile-document)
3. [Become a wizzard](#become-a-wizzard)

### <a name="installation-of-additional-libs"></a> Installation of TeXLive or MikTeX

Firstly you need to install La(Tex) libraries --- If you have *TexLive*, *MikTeX* or similar libraries packages system all ready installed and also biber as well, you can skip this subpoints ;)

##### Linux
For most linux distributions based on [Debian](https://www.debian.org/) like: (ex: [Ubuntu](http://www.ubuntu.com/), [Mint](http://www.linuxmint.com/), [ElementaryOS](http://elementaryos.org/), etc) you could install **texlive** by run this command in terminal:

    sudo apt-get install texlive-full

**apt-get** program will install whole bunch of *Tex* libs (~1.5GB). It's a lot of stuff and contains more libs then we'll ever use, but the main advantage of this setp is that you probably won't get any errors like missing common libraries while you'll be writting your thesis --- less errors, less worries - simple as that ;)

##### Windows
You can download [Miktex](http://miktex.org/). It's more-less an equivalent of *Texlive* libs and it's build specially for Windows systems. On Miktex's website you will find all information you need to use this piece of software.

### <a name="copy-repo"></a>Copy this repository to your hard drive:

    git clone git@github.com:egel/latex-example.git

### <a name="compile-document"></a>Compile whole document:

    $ pdflatex document_main.tex
    $ pdflatex document_main.tex
    $ biber bibliografia.tex
    $ pdflatex document_main.tex

### <a name="become-a-wizzard"></a>Become a wizzard
All done :)  ...and now you can give a try to become a powerful wizard of LaTeX!


## Class options

> Options are Case Sensitive (so there is a difference between `indexNumber` and `indexnumber`)

  - `twoside` --- sets type of printing of the document (**default value is**: oneside);

  - `male` or `female` --- sets all generall setting (like ex: author statement) to men or women preferences (**default value is**: male);

  - `fileVersion` --- print version of document (**default value is**: off); <br/>It's usually used only for helping purposes. This option is defined by `\globalVersion` variable.

  - `indexNumber` --- print student's index number; (**default value is**: off) <br/>This option is defined by `\globalIndexNumber` variable.

  - `keywords` --- print keywords in PDF; (**default value is**: off) <br/>This option is defined by `\globalKeywords` variable. In the backend it also includes these keywords to generated file by *pdflatex* by default. To see more generate PDF and look into file's preferences > document.


#### Additional class commands:

##### Captionsource

`captionsource` --- this command provide convenient way for including caption for many latex objects(tables, figures, math, ect.) with additional source field of where it came from.

    \captionsource{<text-of-the-caption>}{<text-of-the-source>}


##### Lowercase and uppercase references

   - `\lnameref` --- this command provides a reference to the latex's object and print it in lowercase.

   - `\fnameref` --- this command provides a reference to the latex's object and print it in uppercase.

> The important, additional command which do the job is `\nmu`. Its handle of setting uppercase or lowercase in letters.

**Simple example**:

Setter:

    \chapter{\nmu BibTeX \nmu Is \nmu Very \nmu Old}
    \label{chap:this_is_chapter_one}


Reference:

    ...something reference to chapter \lnameref{chap:this_is_chapter_one}.


## Maintenance
Quick remove unnecessary files from compilation folder (into terminal)

    $ rm *.aux *.log *.toc *.lot *.lol *.lot *.out *.dvi *.bbl *.bcf *.blg *.run.xml *.lof *.gz *.gz\(busy\) *.xref *.tmp *.4tc



## <a name="generall-editors-rules"></a> Generall Editor's Rules
It's acctualy not a standard (because every single university has its own rules), but generally many types of thesis implements these rules commonly, so I try to call it a standard.

> Class `uekthesis.cls` was build specially to the latest specifications for thesis documents accordings to rules of University of Economic in Cracow. <br/>To see more look at ("WYMOGI STAWIANE PRACOM DYPLOMOWYM" from 16.11.2009).

Some generall editor's rules implemented in document are listed below:

  * Paper: A4;
  * Font type: Times new roman (Modern latin);
  * Global font size: 12pt;
  * Global footnote size: 10pt;
  * Freespace between lines: 1.5 of row;
  * Document margins [top,right,down,left]: 2,5cm; 2,0cm; 2,5cm; 3,0cm;
  * Justify the text of document;
  * Includes "Polish" akapits;
  * Continiue numeration in all document;
  * Oneside print;

